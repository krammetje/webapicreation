﻿# Assignment 2 - Data access with SQL Client

## Main Functions

### Character

*POST*
​/api​/v1​/character
Adds a new character to the database

*GET*
​/api​/v1​/character
Gets all the characters from the database

*GET*
​/api​/v1​/character​/{id}
Gets a specific character from the database by their id

*PUT*
​/api​/v1​/character​/{id}
Updates a character in the database

*DELETE*
​/api​/v1​/character​/{id}
Deletes a character from the database


### Franchise

*POST*
​/api​/v1​/franchise
Adds a franchise to the database

*GET*
​/api​/v1​/franchise
Gets all the franchises from the database

*GET*
​/api​/v1​/franchise​/{id}
Gets a specific franchise from the database by their id

*PUT*
​/api​/v1​/franchise​/{id}
Updates a franchise in the database

*DELETE*
​/api​/v1​/franchise​/{id}
Deletes a franchise from the database

*GET*
​/api​/v1​/franchise​/{franchiseId}​/movies
Gets all the movies from a specific franchise by its id

*GET*
​/api​/v1​/franchise​/{franchiseId}​/characters
Gets all the characters from a specific franchise by its id

*PUT*
​/api​/v1​/franchise​/{franchiseId}​/updateMovie
Updates the movies in a franchise


### Movie

*POST*
​/api​/v1​/movie
Add a new movie to the database

*GET*
​/api​/v1​/movie
Returns a list of all movies

*GET*
​/api​/v1​/movie​/{id}
returns the movie with given id.

*PUT*
​/api​/v1​/movie​/{id}
Input a movie object to update movie with specified id in the database

*DELETE*
​/api​/v1​/movie​/{id}
Delete a movie from the database

*GET*
​/api​/v1​/movie​/{movieId}​/characters
Returns a list of characters in a specific movie.

*PUT*
​/api​/v1​/movie​/{movieId}​/updateCharacter
Updates the characters from the movie.