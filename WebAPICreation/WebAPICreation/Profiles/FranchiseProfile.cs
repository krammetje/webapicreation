﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebAPICreation.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {

            CreateMap<Franchise, FranchiseReadDTO>()
                // Turning related movies into int arrays
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.Id).ToArray()));

            //FranchiseCreateDTO -> Franchise
            CreateMap<FranchiseCreateDTO, Franchise>();
            // FranchiseEditDTO->Franchise
            CreateMap<FranchiseEditDTO, Franchise>();

        }
    }
}
