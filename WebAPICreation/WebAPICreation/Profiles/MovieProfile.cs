﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebAPICreation.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie->MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                // Turning related characters into int arrays
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.Id).ToArray()));
            // MovieCreateDTO->Movie
            CreateMap<MovieCreateDTO, Movie>();
            // MovieEditDTO->Movie
            CreateMap<MovieEditDTO, Movie>();

        }
    }

}
