﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICreation
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character<->CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            // Character<->CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
            // Character<->CharacterEditDTO
            CreateMap<Character, CharacterEditDTO>()
                .ReverseMap();
        }
    }
}
