﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;

namespace WebAPICreation
{
    [Route("api/v1/franchise")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {

        private readonly MovieDbContext _context;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public FranchiseController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Adds a franchise to the database
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise(FranchiseCreateDTO franchiseDTO)
        {
            _context.Franchises.Add(_mapper.Map<Franchise>(franchiseDTO));
            await _context.SaveChangesAsync();
            Franchise franchise = _context.Franchises.OrderBy(c => c.Id).Last();
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<FranchiseReadDTO>(franchise));
        }

        /// <summary>
        /// Gets all the franchises from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());
        }

        /// <summary>
        /// Gets a specific franchise from the database by their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all the movies from a specific franchise by its id
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpGet("{franchiseId}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies(int franchiseId)
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Where(s => s.FranchiseId == franchiseId)
                                                                        .ToListAsync());
        }

        /// <summary>
        /// Gets all the characters from a specific franchise by its id 
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpGet("{franchiseId}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int franchiseId)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.Include(s => s.Movies)
                .Where(m => m.Movies.Any(p => p.FranchiseId == franchiseId))
                .ToListAsync());

        }

        /// <summary>
        /// Updates a franchise in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Franchises.Any(e => e.Id == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Updates the movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{franchiseId}/updateMovie")]
        public async Task<IActionResult> UpdateMovies(int franchiseId, List<int> movies)
        {
            Franchise franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
            {
                return NotFound();
            }

            foreach (Movie movie in _context.Movies.Where(m => m.FranchiseId == franchiseId).ToList())
            {
                movie.FranchiseId = null;
            }

            List<Movie> movieList = new();
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                movie.FranchiseId = franchiseId;
                movieList.Add(movie);
            }
            franchise.Movies = movieList;
            await _context.SaveChangesAsync();

            return NoContent();

        }

        /// <summary>
        /// Deletes a franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
