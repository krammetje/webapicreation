﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;

namespace WebAPICreation
{
    [Route("api/v2/franchise")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseV2Controller : ControllerBase
    {

        private readonly IFranchiseService _franchiseService;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public FranchiseV2Controller(IFranchiseService franchiseSevice, IMapper mapper)
        {
            _franchiseService = franchiseSevice;
            _mapper = mapper;
        }

        /// <summary>
        /// Adds a franchise to the database
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise(FranchiseCreateDTO franchiseDTO)
        {

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);

            await _franchiseService.CreateFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Gets all the franchises from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Gets a specific franchise from the database by their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all the movies from a specific franchise by its id
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpGet("{franchiseId}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies(int franchiseId)
        {
            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetAllMoviesFromFranchise(franchiseId));
        }

        /// <summary>
        /// Gets all the characters from a specific franchise by its id 
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpGet("{franchiseId}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int franchiseId)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetAllCharactersFromFranchise(franchiseId));
        }

        /// <summary>
        /// Updates a franchise in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Updates the movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{franchiseId}/updateMovie")]
        public async Task<IActionResult> UpdateMovies(int franchiseId, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }

            await _franchiseService.UpdateMoviesAsync(franchiseId, movies);

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
    }
}
