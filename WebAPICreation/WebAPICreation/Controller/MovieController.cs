﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;

namespace WebAPICreation
{
    [Route("api/v1/movie")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MovieController : ControllerBase
    {
        private readonly MovieDbContext _context;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public MovieController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Add a new movie to the database
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostCharacter(MovieCreateDTO movieDTO)
        {
            _context.Movies.Add(_mapper.Map<Movie>(movieDTO));
            await _context.SaveChangesAsync();
            Movie movie = _context.Movies.OrderBy(c => c.Id).Last();
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Returns a list of all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Include(m => m.Characters).ToListAsync());
        }

        /// <summary>
        /// returns the movie with given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.Include(m => m.Characters).FirstAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Returns a list of characters in a specific movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpGet("{movieId}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.Include(c => c.Movies).Where(s => s.Movies.Contains(movie))
                                                                        .ToListAsync());
        }

        /// <summary>
        /// Input a movie object to update movie with specified id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            Movie domainMovie = _mapper.Map<Movie>(movie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Movies.Any(e => e.Id == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Updates the characters from the movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        [HttpPut("{movieId}/updateCharacter")]
        public async Task<IActionResult> UpdateCharacter(int movieId, List<int> characters)
        {
            Movie movie = await _context.Movies.Include(m => m.Characters).FirstAsync(m => m.Id == movieId);
            if (movie == null)
            {
                return NotFound();
            }
            movie.Characters.Clear();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                movie.Characters.Add(character);
            }

            await _context.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Delete a movie from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
