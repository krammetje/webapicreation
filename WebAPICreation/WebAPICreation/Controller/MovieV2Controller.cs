﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;

namespace WebAPICreation
{
    [Route("api/v2/movie")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MovieV2Controller : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IMovieService _movieService;

        public MovieV2Controller(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Add a new movie to the database
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie(MovieCreateDTO movieDTO)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDTO);
            await _movieService.CreateMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Returns a list of all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());

        }

        /// <summary>
        /// returns the movie with given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Returns a list of characters in a specific movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpGet("{movieId}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int movieId)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetAllCharactersFromMovie(movieId));
        }

        /// <summary>
        /// Input a movie object to update movie with specified id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Updates the characters from the movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        [HttpPut("{movieId}/updateCharacter")]
        public async Task<IActionResult> UpdateCharacter(int movieId, List<int> characters)
        {

            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }

            await _movieService.UpdateCharactersAsync(movieId, characters);

            return NoContent();
        }


            /// <summary>
            /// Delete a movie from the database
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
    }
}
