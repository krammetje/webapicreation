﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;

namespace WebAPICreation
{
    class FranchiseServise : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseServise(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> CreateFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).FirstAsync(f => f.Id == id);
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesFromFranchise(int franchiseId)
        {
            return await _context.Movies.Where(s => s.FranchiseId == franchiseId).ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersFromFranchise(int franchiseId)
        {
            return await _context.Characters.Include(s => s.Movies)
                .Where(m => m.Movies.Any(p => p.FranchiseId == franchiseId))
                .ToListAsync();
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync(); 
        }

        public async Task UpdateMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise franchise = await _context.Franchises.FindAsync(franchiseId);
            foreach (Movie movie in _context.Movies.Where(m => m.FranchiseId == franchiseId).ToList())
            {
                movie.FranchiseId = null;
            }
           
            List<Movie> movieList = new();
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                movie.FranchiseId = franchiseId;
                movieList.Add(movie);
            }
            franchise.Movies = movieList;
            await _context.SaveChangesAsync();

        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
