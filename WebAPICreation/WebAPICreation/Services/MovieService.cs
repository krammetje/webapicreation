﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPICreation
{
    class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> CreateMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).FirstAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharactersAsync(int movieId, List<int> characters)
        {
            Movie movie = await _context.Movies.Include(m => m.Characters).FirstAsync(m => m.Id == movieId);
            movie.Characters.Clear();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                movie.Characters.Add(character);
            }

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersFromMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            return await _context.Characters.Include(c => c.Movies).Where(s => s.Movies.Contains(movie)).ToListAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.Id == id);
        }
    }
}
