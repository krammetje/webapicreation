﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace WebAPICreation
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<Franchise> CreateFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task<IEnumerable<Character>> GetAllCharactersFromFranchise(int franchiseId);
        public Task<IEnumerable<Movie>> GetAllMoviesFromFranchise(int franchiseId);
        public Task UpdateMoviesAsync(int franchiseId, List<int> movies);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
