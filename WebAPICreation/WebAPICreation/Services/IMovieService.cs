﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace WebAPICreation
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> CreateMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task<IEnumerable<Character>> GetAllCharactersFromMovie(int id);
        public Task UpdateCharactersAsync(int franchiseId, List<int> movies);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
