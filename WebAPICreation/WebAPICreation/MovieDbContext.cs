﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebAPICreation
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        public MovieDbContext()
        {

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=localhost;Database=MovieDB;Trusted_Connection=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 1,
                    Name = "Lord of the Rings",
                    Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien."
                });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "Marvel Cinematic Universe",
                    Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios."
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    Title = "The Fellowship of the Ring",
                    Genre = "Adventure, Fantasy",
                    Year = 2001,
                    Director = "Peter Jackson",
                    Picture = "https://en.wikipedia.org/wiki/File:The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_(2001).jpg",
                    Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    Title = "The Two Towers",
                    Genre = "Adventure, Fantasy",
                    Year = 2002,
                    Director = "Peter Jackson",
                    Picture = "https://en.wikipedia.org/wiki/File:Lord_of_the_Rings_-_The_Two_Towers_(2002).jpg",
                    Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU",
                    FranchiseId = 1
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    Title = "The Return of the King",
                    Genre = "Adventure, Fantasy",
                    Year = 2003,
                    Director = "Peter Jackson",
                    Picture = "https://en.wikipedia.org/wiki/File:The_Lord_of_the_Rings_-_The_Return_of_the_King_(2003).jpg",
                    Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo",
                    FranchiseId = 1
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 4,
                    Title = "Iron Man",
                    Genre = "Adventure, Action",
                    Year = 2008,
                    Director = "Jon Favreau",
                    Picture = "https://en.wikipedia.org/wiki/File:Iron_Man_(2008_film)_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                    FranchiseId = 2
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 1,
                    FullName = "Frodo Baggins",
                    Alias = "Mr Underhill",
                    Gender = "Male",
                    Picture = "https://en.wikipedia.org/wiki/File:Elijah_Wood_as_Frodo_Baggins.png"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 2,
                    FullName = "Gandalf",
                    Alias = "The grey",
                    Gender = "Male",
                    Picture = "https://en.wikipedia.org/wiki/File:Gandalf600ppx.jpg"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 3,
                    FullName = "Tony Stark",
                    Alias = "Iron man",
                    Gender = "Male",
                    Picture = "https://en.wikipedia.org/wiki/File:Robert_Downey_Jr_at_Comic_Con_2007.jpg"
                });


            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 3 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 },
                            new { CharacterId = 3, MovieId = 4 }
                        );
                    });
        }
    }
}
