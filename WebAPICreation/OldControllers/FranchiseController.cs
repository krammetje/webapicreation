﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPICreation.Controller
{
    public static class FranchiseController
    {
        public static void AddFranchise(Franchise paramFranchise)
        {
            using (MovieContext context = new MovieContext())
            {
                context.Franchises.Add(paramFranchise);
                context.SaveChanges();
            }
        }

        public static Franchise GetFranchise(int id)
        {
            using (MovieContext context = new MovieContext())
            {
                Franchise franchise = context.Franchises.SingleOrDefault(s => s.Id == id);
                return franchise;
            }
        }

        public static List<Movie> GetMovies(int franchiseId)
        {
            using (MovieContext context = new MovieContext())
            {
                Franchise franchise = context.Franchises.Find(franchiseId);
                List<Movie> movies = context.Movies.Where(s => s.FranchiseId == franchiseId).ToList();
                return movies;
            }
        }

        public static List<Character> GetCharacters(int franchiseId)
        {
            using (MovieContext context = new MovieContext())
            {
                Franchise franchise = context.Franchises.Find(franchiseId);
                List<Character> movies = context.Characters.Include(s => s.Movies).Where(m => m.Movies.Any(p => p.FranchiseId == franchiseId)).ToList();
                return movies;
            }
        }

        public static void UpdateFranchise(int id, Franchise newData)
        {
            using (MovieContext context = new MovieContext())
            {
                var franchise = context.Franchises.Find(id);
                franchise.Name = newData.Name != null ? newData.Name : franchise.Name;
                franchise.Description = newData.Description != null ? newData.Description : franchise.Description;
                context.SaveChanges();
            }
        }

        public static void DeleteFranchise(int id)
        {
            using (MovieContext context = new MovieContext())
            {
                var franchise = context.Franchises.Find(id);
                context.Remove(franchise);
                context.SaveChanges();
            }
        }
    }
}
