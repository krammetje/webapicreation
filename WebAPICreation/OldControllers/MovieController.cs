﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPICreation.Controller
{
    public static class MovieController
    {
        public static void AddMovie(Movie paramMovie)
        {
            using (MovieContext context = new MovieContext())
            {
                context.Movies.Add(paramMovie);
                context.SaveChanges();
            }
        }

        public static Movie GetMovie(int id)
        {
            using (MovieContext context = new MovieContext())
            {
                Movie movie = context.Movies.SingleOrDefault(s => s.Id == id);
                return movie;
            }
        }

        public static List<Character> GetCharacters(int movieId)
        {
            using (MovieContext context = new MovieContext())
            {
                Movie movie = context.Movies.Find(movieId);
                List<Character> characters = context.Characters.Where(s => s.Movies.Contains(movie)).ToList();
                return characters;
            }
        }

        public static void UpdateMovie(int id, Movie newData)
        {
            using (MovieContext context = new MovieContext())
            {
                var movie = context.Movies.Find(id);
                movie.Title = newData.Title != null ? newData.Title : movie.Title;
                movie.Genre = newData.Genre != null ? newData.Genre : movie.Genre;
                movie.Year = newData.Year > 0 ? newData.Year: movie.Year;
                movie.Director= newData.Director != null ? newData.Director: movie.Director;
                movie.Picture = newData.Picture != null ? newData.Picture : movie.Picture;
                movie.Trailer = newData.Trailer != null ? newData.Trailer : movie.Trailer;
                //movie.Franchise = newData.Franchise != null ? newData.Franchise : movie.Franchise;
                context.SaveChanges();
            }
        }

        public static void DeleteMovie(int id)
        {
            using (MovieContext context = new MovieContext())
            {
                var movie = context.Movies.Find(id);
                context.Remove(movie);
                context.SaveChanges();
            }
        }
    }
}
