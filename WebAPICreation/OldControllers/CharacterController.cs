﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPICreation.Controller
{
    public static class CharacterController
    {
        public static void AddCharacter(Character paramChar)
        {
            using (MovieContext context = new MovieContext())
            {
                context.Characters.Add(paramChar);
                context.SaveChanges();
            }
        }

        public static Character GetCharacter(int id)
        {
            using(MovieContext context = new MovieContext())
            {
                Character character = context.Characters.SingleOrDefault(s => s.Id == id);
                return character;
            }
        }

        public static List<Character> GetCharacters()
        {
            using (MovieContext context = new MovieContext())
            {
                List<Character> characters = context.Characters.ToList();
                return characters;
            }
        }

        public static void UpdateCharacter(int id, Character newData)
        {
            using (MovieContext context = new MovieContext())
            {
                var character = context.Characters.Find(id);
                character.FullName = newData.FullName != null ? newData.FullName: character.FullName;
                character.Alias = newData.Alias != null ? newData.Alias: character.Alias;
                character.Gender = newData.Gender != null ? newData.Gender: character.Gender;
                character.Picture = newData.Picture != null ? newData.Picture: character.Picture;
                context.SaveChanges();
            }
        }

        public static void DeleteCharacter(int id)
        {
            using (MovieContext context = new MovieContext())
            {
                var character = context.Characters.Find(id);
                context.Remove(character);
                context.SaveChanges();
            }
        }


    }
}
